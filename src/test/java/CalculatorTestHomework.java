import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.Assertions.Calculator;


//HOMEWORK


public class CalculatorTestHomework {

    static Calculator d;

    @BeforeClass
    public static void beforeTest() {
        d = new Calculator();
    }


    //ADDITION
    @Test
    public void testAddition1() {
        Assert.assertEquals(11, d.compute(9.1, 1.9, "+"), 0);
    }

    @Test
    public void testAddition2() {
        Assert.assertEquals(-30, d.compute(-29, -1, "+"), 0);
    }

    @Test
    public void testAddition3() {
        Assert.assertEquals(5954, d.compute(5525, 429, "+"), 0);
    }



    //SUBSTRACTION
    @Test
    public void testSubstract1() {
        Assert.assertEquals(-10, d.compute(-15, -5, "-"), 0);
    }

    @Test
    public void testSubstract2() {
        Assert.assertEquals(45, d.compute(50, 5, "-"), 0);
    }

    @Test
    public void testSubstract3() {
        Assert.assertEquals(-3455, d.compute(9000, 12455, "-"), 0);
    }



    //MULTIPLICATION
    @Test
    public void testMultiply1() {
        Assert.assertEquals(0, d.compute(100, 0, "*"), 0);
    }

    @Test
    public void testMultiply2() {
        Assert.assertEquals(78750, d.compute(875, 90, "*"), 0);
    }

    @Test
    public void testMultiply3() {
        Assert.assertEquals(-45, d.compute(-9, 5, "*"), 0);
    }



    //DIVISION
    @Test
    public void testDivision1() {
        Assert.assertEquals(30, d.compute(-90, -3, "/"), 0);
    }

    @Test
    public void testDivision2() {
        Assert.assertEquals(0, d.compute(0, -1, "/"), 0);
    }

    @Test
    public void testDivision3() {
        Assert.assertEquals(1, d.compute(5000, 4940, "/"), 0.1);
    }



    //SQRT
    @Test
    public void testSQRT1() {
        Assert.assertEquals(7.58, d.compute(57.45, 0, "SQRT"), 0.001);
    }

    @Test
    public void testSQRT2() {
        Assert.assertEquals(5871, d.compute(34468641, 0, "SQRT"), 0.01);
    }

    @Test
    public void testSQRT3() {
        Assert.assertEquals(1, d.compute(1, 1, "SQRT"),0);
    }


}






