package ro.siit.curs5.Homework_4;

public class BuildingMain {

    public static void main(String[] args) {
        System.out.println("This building has " + enumFloors.values().length + " floors.");
        System.out.println();
        Floor1.Floor1Stuff();
        Floor2.Floor2Stuff();
        Floor3.Floor3Stuff();
    }

    public enum enumFloors {
        FLOOR1,
        FLOOR2,
        FLOOR3
    }

    int office, kitchen, conferenceRoom, toilet;

    public BuildingMain(int office, int kitchen, int conferenceRoom, int toilet) {
        this.office = office;
        this.kitchen = kitchen;
        this.conferenceRoom = conferenceRoom;
        this.toilet = toilet;
    }

    public BuildingMain(int conferenceRoom, int toilet) {
        this.conferenceRoom = conferenceRoom;
        this.toilet = toilet;
    }


    public int getOffice() {
        return office;
    }

    public void setOffice(int office) {
        this.office = office;
    }

    public int getKitchen() {
        return kitchen;
    }

    public void setKitchen(int kitchen) {
        this.kitchen = kitchen;
    }

    public int getConferenceRoom() {
        return conferenceRoom;
    }

    public void setConferenceRoom(int conferenceRoom) {
        this.conferenceRoom = conferenceRoom;
    }

    public int getToilet() {
        return toilet;
    }

}
