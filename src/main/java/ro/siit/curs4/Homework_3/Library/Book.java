package ro.siit.curs4.Homework_3.Library;

public class Book {
    public String name;
    public int year;
    public double price;
    public Author author;


    public Book(String name, int year, Author author, double price) {
        this.name = name;
        this.year = year;
        this.price = price;
        this.author = author;
    }
    public String getName(){
        return name;
    }

    public int getYear(){
        return year;
    }

    public double getPrice(){
        return price;
    }

    public Author getAuthor() {
        return author;
    }




}
